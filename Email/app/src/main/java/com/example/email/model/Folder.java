package com.example.email.model;

import java.util.ArrayList;
import java.util.List;


public class Folder {

	private int id;
	private String name;
	private List<Message> messages = new ArrayList<>();
	private Folder folder;
	private List<Folder> folders = new ArrayList<>();
	private List<Rule> rules = new ArrayList<>();
	private Account account;


	public Folder(){

	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Message> getMessages(){ return messages;}
	public void setMessages(List<Message> messages) { this.messages = messages; }

	public List<Folder> getFolders(){ return folders;}
	public void setFolders(List<Folder> folders) { this.folders = folders; }

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public List<Rule> getRules(){ return rules;}
	public void setRules(List<Rule> rules) { this.rules = rules; }


	public Account getAccount() {
		return account;
	}


	public void setAccount(Account account) {
		this.account = account;
	}


}
