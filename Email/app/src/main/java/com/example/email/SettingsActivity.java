package com.example.email;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.email.data.Data;
import com.example.email.model.Account;
import com.example.email.model.Message;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class SettingsActivity extends PreferenceActivity {

    public static List<Message> novaListaPoruka = new ArrayList<>();
    String currValue1 = "-1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        ListPreference listPreference = (ListPreference) findPreference("osv");
        CharSequence currText = listPreference.getEntry();
        String currValue = listPreference.getValue();
        ListPreference listPreference1 = (ListPreference) findPreference("sort");
        CharSequence currText1 = listPreference1.getEntry();
        currValue1 = listPreference1.getValue();

        LoginActivity.userDetails = LoginActivity.sharedPreferences.getString("osvezavanje", currValue);
        LoginActivity.editor = LoginActivity.sharedPreferences.edit();
        LoginActivity.editor.putString("osvezavanje1",LoginActivity.userDetails);
        LoginActivity.editor.commit();

        LoginActivity.userDetails = LoginActivity.sharedPreferences.getString("datum", currValue1);
        LoginActivity.editor = LoginActivity.sharedPreferences.edit();
        LoginActivity.editor.putString("datum1",LoginActivity.userDetails);
        LoginActivity.editor.commit();
        Log.i("AAAAAAAAAAA",LoginActivity.sharedPreferences.getAll().toString());



        if(Integer.parseInt(LoginActivity.sharedPreferences.getString("osvezavanje1","")) != 0){
            int timeout = Integer.parseInt(LoginActivity.sharedPreferences.getString("osvezavanje1","")) * 1000;

            Timer timer = new Timer();
            timer.schedule(new TimerTask() {

                @Override
                public void run() {
                    MainActivity.RetrofitMessages();
                    MainActivity.RetrofitAttachments();
                }
            }, timeout);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        /*List<String> dates = new ArrayList<>();
        for(Message m : EmailsActivity.accountMessages){
            dates.add(m.getDateTime());
        }
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        List<Date> ddates = new ArrayList<>();
        for(String d : dates){
            try{
                Date f;
                f = format.parse(d);
                ddates.add(f);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }


        if(Integer.parseInt(currValue1) == 0){
            Collections.sort(ddates, Collections.<Date>reverseOrder());



            Date stringToDate;

            for(Date l : ddates){
                for(Message m : EmailsActivity.accountMessages){
                    try{
                        stringToDate = format.parse(m.getDateTime());
                        if(stringToDate.compareTo(l) ==0 ){
                            novaListaPoruka.add(m);
                        }
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            EmailsActivity.accountMessages = novaListaPoruka;
        }
        else{
            Collections.sort(ddates);


            Date stringToDate;

            for(Date l : ddates){
                for(Message m : EmailsActivity.accountMessages){
                    try{
                        stringToDate = format.parse(m.getDateTime());
                        if(stringToDate.compareTo(l) == 0){
                            novaListaPoruka.add(m);
                        }
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
            }

            EmailsActivity.accountMessages = novaListaPoruka;
        }*/


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
