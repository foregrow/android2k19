package com.example.email;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.data.RestService;
import com.example.email.data.Url;
import com.example.email.model.Account;
import com.example.email.model.Contact;
import com.example.email.model.Folder;
import com.example.email.model.Message;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpdateEmailActivity extends AppCompatActivity {

    EditText etnaslov;
    MultiAutoCompleteTextView ettoo;
    MultiAutoCompleteTextView etcc;
    MultiAutoCompleteTextView etbcc;
    EditText etporuka;
    String naslov = "";
    String too = "";
    String cc = "";
    String bcc = "";
    String poruka = "";
    int idPoruke = -1;
    Retrofit retrofit;
    RestService restService;
    List<String> EMAILS = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_email);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("ID");
        for(Message m: Data.emails){
            if(m.getId() == Integer.parseInt(id)){
                idPoruke = m.getId();
                etnaslov = findViewById(R.id.editTextNaslov);
                ettoo = findViewById(R.id.editTextTO);
                etcc = findViewById(R.id.editTextCC);
                etbcc = findViewById(R.id.editTextBCC);
                etporuka = findViewById(R.id.editTextSadrzaj);

                etnaslov.setText(m.getSubject());
                String to = "";
                for(String s : m.getTo()){
                    to += s + ",";
                }
                ettoo.setText(to);
                String cc = "";
                for(String s : m.getCc()){
                    cc += s + ",";
                }
                etcc.setText(cc);
                String bcc = "";
                for(String s : m.getBcc()){
                    bcc += s + ",";
                }
                etbcc.setText(bcc);
                etporuka.setText(m.getContent());
                //fali attachment

            }
        }
        punjenje();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, EMAILS);
        ettoo.setAdapter(adapter);
        ettoo.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        etcc.setAdapter(adapter);
        etcc.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        etbcc.setAdapter(adapter);
        etbcc.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.update_email_menu, menu);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {

            case R.id.cancelUpdate:
                retrofit = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                restService = retrofit.create(RestService.class);
                Call<Void> callv = restService.deleteMessage(idPoruke);
                callv.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if(!response.isSuccessful()){
                            Log.i("RESPONSE CODE",Integer.toString(response.code()));
                            return;
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

                Message mess1=null;
                for(Folder f : Data.folders){
                    if(f.getId() == 10){
                        for(Message m :f.getMessages()){
                            if(idPoruke == m.getId()){
                                mess1 = m;
                            }
                        }
                    }
                }

                if(mess1 != null){
                    Data.emails.remove(mess1);
                    for(Folder f : Data.folders){
                        f.getMessages().remove(mess1);
                    }
                }

                Toast.makeText(this, "Poruka obrisana!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this,FoldersActivity.class);
                startActivity(intent);
                return true;

            case R.id.sendUpdate:
                naslov = etnaslov.getText().toString().trim();
                too = ettoo.getText().toString().trim();
                cc = etcc.getText().toString().trim();
                bcc = etbcc.getText().toString().trim();
                poruka = etporuka.getText().toString().trim();
                //Toast.makeText(this,too, Toast.LENGTH_SHORT).show();

                if(naslov.equals("")){
                    Toast.makeText(this, "Polje naslov ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(too.equals("")){
                    Toast.makeText(this, "Polje TO ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(poruka.equals("")){
                    Toast.makeText(this, "Polje sadrzaj ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }

                retrofit = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                restService = retrofit.create(RestService.class);

                String loggedIn = LoginActivity.sharedPreferences.getString("display", "");

                String[] toInputs = too.split("\\s*,\\s*");
                String[] ccInputs = null;
                if(!cc.equals("")){
                    ccInputs = cc.split("\\s*,\\s*");
                }
                String[] bccInputs = null;
                if(!bcc.equals("")){
                    bccInputs = bcc.split("\\s*,\\s*");
                }

                java.text.SimpleDateFormat sdf =
                        new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Calendar now = Calendar.getInstance();

                String date = sdf.format(now.getTime());

                Message mess = null;
                for(Message m : Data.emails){
                    if(m.getId() == idPoruke){

                        m.setSubject(naslov);
                        m.setContent(poruka);
                        m.setDateTime(date);
                        m.setFolder(null);
                        for(String s : toInputs){
                            m.getTo().add(s);
                        }
                        if(ccInputs != null){
                            for(String s : ccInputs){
                                m.getCc().add(s);
                            }
                        }
                        if(bccInputs != null){
                            for(String s : bccInputs){
                                m.getBcc().add(s);
                            }
                        }
                        mess = m;
                        break;
                    }
                }
                Message m = null;
                for(Folder f : FoldersActivity.accountFolders){
                    if(f.getId() == 10){
                        for(Message me : f.getMessages()){
                            if(mess.getId() == me.getId()){
                                m = me;
                            }
                        }
                    }
                }
                if(m != null){
                    for(Folder f : FoldersActivity.accountFolders){
                        if(f.getId() == 10){
                            f.getMessages().remove(m);
                        }

                    }
                }

                if(cc.equals("")){
                    cc = "-1";
                }
                if(bcc.equals("")){
                    bcc = "-1";
                }
                //Call<Message> call = restService.createMessage(message.getId(), message.getSubject(), message.getTo(), message.getCc(), message.getBcc(), message.getDateTime(), message.getContent());
                Call<Message> call = restService.updateMessage(idPoruke, mess.getFrom(), too,cc,bcc,mess.getDateTime(),naslov,poruka);
                call.enqueue(new Callback<Message>() {
                    @Override
                    public void onResponse(Call<Message> call, Response<Message> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }

                        Message m = response.body();
                    }

                    @Override
                    public void onFailure(Call<Message> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

                Intent inm = new Intent(this,EmailsActivity.class);
                startActivity(inm);
                return true;
    }
        return super.onOptionsItemSelected(item);
    }

    private void punjenje(){

        for(Contact c : EmailsActivity.accountContacts){
            EMAILS.add(c.getEmail());
        }

    }
}
