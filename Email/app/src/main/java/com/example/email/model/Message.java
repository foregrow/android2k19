package com.example.email.model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Message {



	@SerializedName("id")
	private int id;

	@SerializedName("from")
	private String from; //email onog ko salje
	@SerializedName("to")
	private List<String> to = new ArrayList<>(); //email onoga kome se salje
	@SerializedName("cc")
	private List<String> cc = new ArrayList<>(); //email onoga kome se salje
	@SerializedName("bcc")
	private List<String> bcc = new ArrayList<>(); //email onoga kome se salje

	@SerializedName("dateTime")
	private String dateTime;

	@SerializedName("subject")
	private String subject;

	@SerializedName("content")
	private String content;
	@SerializedName("folder")
	private Folder folder;
	@SerializedName("tags")
	private List<Tag> tags = new ArrayList<>();
	@SerializedName("attachments")
	private List<Attachment> attachments = new ArrayList<>();
	@SerializedName("account")
	private Account account;
	@SerializedName("checked")
	private boolean checked = false;



	public Message(){

	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public List<String> getTo() {
		return to;
	}

	public void setTo(List<String> to) {
		this.to = to;
	}

	public List<String> getCc() {
		return cc;
	}

	public void setCc(List<String> cc) {
		this.cc = cc;
	}

	public List<String> getBcc() {
		return bcc;
	}

	public void setBcc(List<String> bcc) {
		this.bcc = bcc;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}


	public boolean isChecked() {
		return checked;
	}


	public void setChecked(boolean checked) {
		this.checked = checked;
	}

}