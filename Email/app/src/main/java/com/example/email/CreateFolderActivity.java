package com.example.email;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.data.RestService;
import com.example.email.data.Url;
import com.example.email.model.Account;
import com.example.email.model.Condition;
import com.example.email.model.Folder;
import com.example.email.model.Operation;
import com.example.email.model.Rule;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateFolderActivity extends AppCompatActivity {

    EditText etName;
    int id;
    String name;
    Folder newFolder;
    Rule r;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_folder);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_folder_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.cancelCreateFolder:
                Intent in2 = new Intent(this,FoldersActivity.class);
                startActivity(in2);
                return true;
            case R.id.saveFolder:

                etName = findViewById(R.id.etFolder);
                EditText etCondition = findViewById(R.id.etCondition);
                EditText etOperation = findViewById(R.id.etOperation);
                EditText etValue = findViewById(R.id.etRuleValue);

                name = etName.getText().toString();
                String cond = etCondition.getText().toString();
                String oper = etOperation.getText().toString();
                String value = etValue.getText().toString();

                List<String> listOper = new ArrayList<>();
                List<String> listCond = new ArrayList<>();
                listCond.add("FROM");
                listCond.add("TO");
                listCond.add("CC");
                listCond.add("SUBJECT");

                listOper.add("MOVE");
                listOper.add("DELETE");
                listOper.add("COPY");

                if(name.equals("")){
                    Toast.makeText(this, "Ime foldera ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(!listCond.contains(cond)){
                    Toast.makeText(this, "Condition mora biti FROM, TO, CC ili SUBJECT", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(!listOper.contains(oper)){
                    Toast.makeText(this, "Operation mora biti MOVE, COPY ili DELETE", Toast.LENGTH_SHORT).show();
                    return true;
                }


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                RestService restService = retrofit.create(RestService.class);

                String loggedIn = LoginActivity.sharedPreferences.getString("display", "");

                newFolder = new Folder();
                id = hashCode();
                newFolder.setId(id);
                newFolder.setName(name);
                for(Account a : Data.accounts){
                    if(a.getUsername().equals(loggedIn)){
                        newFolder.setAccount(a);
                    }
                }

                r = new Rule();
                r.setCondition(Condition.valueOf(cond));
                r.setOperation(Operation.valueOf(oper));
                r.setValue(value);
                r.setFolder(newFolder);

                /*Map<String, String> fields = new HashMap<>();
                fields.put("id", Integer.toString(id));
                fields.put("firstName", ime);
                fields.put("lastName",prezime);
                fields.put("display",display);
                fields.put("email",email);*/


                Call<Folder> call = restService.createFolder(id,name,Condition.valueOf(cond),Operation.valueOf(oper),value,loggedIn);

                call.enqueue(new Callback<Folder>() {
                    @Override
                    public void onResponse(Call<Folder> call, Response<Folder> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<Folder> call, Throwable t) {
                        Log.i("STACK","TRACE");
                        t.printStackTrace();
                    }
                });
                //MainActivity.RetrofitFolders();
                Data.folders.add(newFolder);
                Data.rules.add(r);

                Intent in = new Intent(this,FoldersActivity.class);
                startActivity(in);

                return true;

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
