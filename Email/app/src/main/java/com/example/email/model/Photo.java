package com.example.email.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Photo {

	@SerializedName("id")
	@Expose
	private int id;
	@SerializedName("path")
	@Expose
	private	String path;
	@SerializedName("contact")
	@Expose
	private int contact;

	public Photo(){

	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getContact() {
		return contact;
	}

	public void setContact(int contact) {
		this.contact = contact;
	}


}