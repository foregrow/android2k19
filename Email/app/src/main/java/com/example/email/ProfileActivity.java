package com.example.email;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.model.Account;

public class ProfileActivity extends AppCompatActivity {

    private DrawerLayout mLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mLinearLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        TextView ime = findViewById(R.id.txtProfilIme);
        TextView prezime = findViewById(R.id.txtProfilPrezime);
        TextView email = findViewById(R.id.txtProfilUsername);

        for(Account a : Data.accounts){
            if(LoginActivity.sharedPreferences.getString("display", "").equals(a.getUsername())){
                email.setText(a.getUsername());
            }
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                //int id = menuItem.getItemId();
                String title = menuItem.getTitle().toString();
                Intent intent;
                if(title.equals("Mejlovi")){
                    intent = new Intent(ProfileActivity.this,EmailsActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Kontakti")){
                    intent = new Intent(ProfileActivity.this,ContactsActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Folderi")){
                    intent = new Intent(ProfileActivity.this,FoldersActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Podesavanja")){
                    intent = new Intent(ProfileActivity.this,SettingsActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Odjava")){
                    LoginActivity.editor.remove("display").commit();
                    Log.i("OBRISAN",LoginActivity.sharedPreferences.getAll().toString());
                    Intent intentL = new Intent(getApplicationContext(), LoginActivity.class);
                    intentL.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentL);
                }
                mLinearLayout.closeDrawers();
                return true;

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mLinearLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.logout:
                LoginActivity.editor.remove("display").commit();
                Log.i("OBRISAN",LoginActivity.sharedPreferences.getAll().toString());
                Intent intentL = new Intent(getApplicationContext(), LoginActivity.class);
                intentL.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentL);
                return true;
            case R.id.change_profile:
                Intent intent = new Intent(ProfileActivity.this, ChangeProfileActivity.class);
                startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
