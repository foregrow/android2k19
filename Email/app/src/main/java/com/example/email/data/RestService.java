package com.example.email.data;

import com.example.email.model.Account;
import com.example.email.model.Attachment;
import com.example.email.model.Condition;
import com.example.email.model.Contact;
import com.example.email.model.Folder;
import com.example.email.model.Message;
import com.example.email.model.Operation;
import com.example.email.model.Photo;
import com.example.email.model.Rule;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RestService {

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @GET("messages")
    Call<List<Message>> getMessages();

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })

    @POST("messages/message/{id}/{from}/{to}/{cc}/{bcc}/{dateTime}/{subject}/{content}/{tagsIds}/{tagsNames}/{user}")
    Call<Message> createMessage(
            @Path("id") int id,
            @Path("from") String from,
            @Path("to") String to,
            @Path("cc") String cc,
            @Path("bcc") String bcc,
            @Path("dateTime") String dateTime,
            @Path("subject") String subject,
            @Path("content") String content,
            @Path("tagsIds") String tagsIds,
            @Path("tagsNames") String tagsNames,
            @Path("user") String user
    );

    @POST("messages/message/{id}/{from}/{to}/{cc}/{bcc}/{dateTime}/{subject}/{content}/{tagsIds}/{tagsNames}/{user}/{aIds}/{aNames}/{aDatas}")
    Call<Message> createMessage(
            @Path("id") int id,
            @Path("from") String from,
            @Path("to") String to,
            @Path("cc") String cc,
            @Path("bcc") String bcc,
            @Path("dateTime") String dateTime,
            @Path("subject") String subject,
            @Path("content") String content,
            @Path("tagsIds") String tagsIds,
            @Path("tagsNames") String tagsNames,
            @Path("user") String user,
            @Path("aIds") String aIds,
            @Path("aNames") String aNames,
            @Path("aDatas") String aDatas
    );

    @POST("messages/message/{id}/{from}/{to}/{cc}/{bcc}/{dateTime}/{subject}/{content}/{folder}")
    Call<Message> createMessage(
            @Path("id") int id,
            @Path("from") String from,
            @Path("to") String to,
            @Path("cc") String cc,
            @Path("bcc") String bcc,
            @Path("dateTime") String dateTime,
            @Path("subject") String subject,
            @Path("content") String content,
            @Path("folder") int folder

    );

    @POST("messages/message/{id}/{from}/{to}/{dateTime}/{subject}/{content}")
    Call<Message> createMessage(
            @Path("id") int id,
            @Path("from") String from,
            @Path("to") String to,
            @Path("dateTime") String dateTime,
            @Path("subject") String subject,
            @Path("content") String content
    );

    @PUT("messages/message/{id}/{from}/{to}/{cc}/{bcc}/{dateTime}/{subject}/{content}")
    Call<Message> updateMessage(
            @Path("id") int id,
            @Path("from") String from,
            @Path("to") String to,
            @Path("cc") String cc,
            @Path("bcc") String bcc,
            @Path("dateTime") String dateTime,
            @Path("subject") String subject,
            @Path("content") String content

    );

    @PUT("messages/message/{id}")
    Call<Message> updateMessage(
            @Path("id") int id
    );

    @DELETE("messages/message/{id}")
    Call<Void> deleteMessage(@Path("id") int id);

    @GET("accounts")
    Call<List<Account>> getAccounts();

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @GET("contacts")
    Call<List<Contact>> getContacts();

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @GET("folders")
    Call<List<Folder>> getFolders();

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @GET("rules")
    Call<List<Rule>> getRules();

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @GET("attachments")
    Call<List<Attachment>> getAttachments();



    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @POST("contacts/contact/{id}/{first}/{last}/{email}/{display}/{user}")
    Call<Contact> createContact(
            @Path("id") int id,
            @Path("first") String first,
            @Path("last") String last,
            @Path("email") String email,
            @Path("display") String display,
            @Path("user") String user
            );

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @PUT("contacts/contact/{id}/{first}/{last}/{email}/{display}")
    Call<Contact> updateContact(
            @Path("id") int id,
            @Path("first") String first,
            @Path("last") String last,
            @Path("email") String email,
            @Path("display") String display
    );

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @DELETE("contacts/contact/{id}")
    Call<Void> deleteContact(@Path("id") int id);



    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @POST("folders/folder/{id}/{name}/{cond}/{oper}/{value}/{user}")
    Call<Folder> createFolder(
            @Path("id") int id,
            @Path("name") String name,
            @Path("cond") Condition cond,
            @Path("oper") Operation oper,
            @Path("value") String value,
            @Path("user") String user

);

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @PUT("folders/folder/{id}/{name}/{ruleId}/{condition}/{operation}/{value}")
    Call<Folder> updateFolder(
            @Path("id") int id,
            @Path("name") String name,
            @Path("ruleId") int ruleId,
            @Path("condition") String condition,
            @Path("operation") String operation,
            @Path("value") String value

    );

    @Headers({
            "User-Agent: Mobile-Android",
            "Content-Type:application/json"
    })
    @DELETE("folders/folder/{id}")
    Call<Void> deleteFolder(@Path("id") int id);


    @POST("photos/photo/{id}/{path}/{contact}")
    Call<Photo> createPhoto(
            @Path("id") int id,
            @Path("path") String path,
            @Path("contact") int contact
            );

    @PUT("photos/photo/{id}/{path}")
    Call<Photo> updatePhoto(
            @Path("id") int id,
            @Path("path") String path
    );


}
