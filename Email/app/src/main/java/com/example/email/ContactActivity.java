package com.example.email;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.data.RestService;
import com.example.email.data.Url;
import com.example.email.model.Account;
import com.example.email.model.Contact;
import com.example.email.model.Folder;
import com.example.email.model.Photo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ContactActivity extends AppCompatActivity {

    EditText etIme;
    EditText etPrezime;
    EditText etDisplay;
    EditText etEmail;
    int gId;
    String ime;
    String prezime;
    String display;
    String email;
    Contact contact;

    ImageView contactImage;
    Button chooseImage;
    String photoPath = "";

    int photoId = -1;
    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    Retrofit retrofit;
    RestService restService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView firstName = findViewById(R.id.etContactIme);
        TextView lastName = findViewById(R.id.etContactPrezime);
        TextView email = findViewById(R.id.etContactEmail);
        TextView display = findViewById(R.id.etContactDisplay);

        chooseImage = findViewById(R.id.btnChange);
        contactImage = findViewById(R.id.iwContact);


        Bundle bundle = getIntent().getExtras();
        String sid = bundle.getString("ID");
        Log.i("PRVO","DI " + sid);
        int idd = Integer.parseInt(sid);
        gId = idd;
        for(Contact c : EmailsActivity.accountContacts){
            if(c.getId() == idd){
                if(c.getPhoto() == null){
                    contact = c;
                    firstName.setText(c.getFirstName());
                    lastName.setText(c.getLastName());
                    email.setText(c.getEmail());
                    display.setText(c.getDisplay());
                }
                else{
                    Uri myUri = Uri.parse(c.getPhoto().getPath());
                    contact = c;
                    firstName.setText(c.getFirstName());
                    lastName.setText(c.getLastName());
                    email.setText(c.getEmail());
                    display.setText(c.getDisplay());
                    contactImage.setImageURI(myUri);
                    photoId = c.getPhoto().getId();
                    photoPath = c.getPhoto().getPath();
                }
                break;
            }
        }


        chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_DENIED){
                        //permission not granted
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};

                        requestPermissions(permissions, PERMISSION_CODE);

                    }
                    else{
                        //permission already granted
                        pickImageFromGallery();
                    }
                }
                else {
                    //system os prob
                    pickImageFromGallery();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.contact_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.saveContact:
                etIme = findViewById(R.id.etContactIme);
                etPrezime = findViewById(R.id.etContactPrezime);
                etDisplay = findViewById(R.id.etContactDisplay);
                etEmail = findViewById(R.id.etContactEmail);

                ime = etIme.getText().toString();
                prezime = etPrezime.getText().toString();
                display = etDisplay.getText().toString();
                email = etEmail.getText().toString();

                if(ime.equals("")){
                    Toast.makeText(this, "Ime kontakta ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(prezime.equals("")){
                    Toast.makeText(this, "Prezime kontakta ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(display.equals("")){
                    Toast.makeText(this, "Displej kontakta ne sme ostati prazan!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(email.equals("")){
                    Toast.makeText(this, "Email kontakta ne sme ostati prazan!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(photoPath.equals("")){
                    Toast.makeText(this, "Molimo vas selektujte sliku!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                retrofit = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                restService = retrofit.create(RestService.class);

                Log.i("DRUGO","DI " + gId);
                Call<Contact> call = restService.updateContact(gId,ime,prezime,email,display);

                call.enqueue(new Callback<Contact>() {
                    @Override
                    public void onResponse(Call<Contact> call, Response<Contact> response) {
                        if(!response.isSuccessful()){
                            Log.i("RESPONSE CODE CONTACT", Integer.toString(response.code()));
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<Contact> call, Throwable t) {
                        t.printStackTrace();
                    }
                });



                String enc = Base64.encodeToString(photoPath.getBytes(),
                        Base64.NO_WRAP);


                Retrofit retrofit1 = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                RestService restService1 = retrofit1.create(RestService.class);

                Call<Photo> call1 = restService1.updatePhoto(photoId,enc);

                call1.enqueue(new Callback<Photo>() {
                    @Override
                    public void onResponse(Call<Photo> call, Response<Photo> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE PHOTO", Integer.toString(response.code()));
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<Photo> call, Throwable t) {
                        Log.i("STACK","TRACE photo");
                        t.printStackTrace();
                        return;
                    }
                });
                for(Account a : Data.accounts){
                    if(a.getUsername().equals(LoginActivity.sharedPreferences.getString("display", ""))){
                        for(Contact c : a.getContacts()){
                            if(contact.getId() == c.getId()) {
                                c.setFirstName(ime);
                                c.setLastName(prezime);
                                c.setEmail(email);
                                c.setDisplay(display);
                                c.getPhoto().setPath(photoPath);
                                Log.i("PODACI", c.getFirstName() + c.getLastName() + c.getEmail() + c.getDisplay() + c.getPhoto().getPath());
                                break;
                            }
                        }

                    }
                }
                /*
                for(Contact c : Data.contacts){
                    if(gId == c.getId()){
                        c.setFirstName(ime);
                        c.setLastName(prezime);
                        c.setEmail(email);
                        c.setDisplay(display);
                        c.getPhoto().setPath(photoPath);
                        Log.i("PODACI",c.getFirstName()+c.getLastName()+c.getEmail()+c.getDisplay()+c.getPhoto().getPath());
                    }
                }*/


                Intent in = new Intent(this,ContactsActivity.class);
                startActivity(in);
                return true;
            case R.id.deleteContact:
                retrofit = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                restService = retrofit.create(RestService.class);
                Call<Void> callv = restService.deleteContact(contact.getId());
                callv.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if(!response.isSuccessful()){
                            Log.i("RESPONSE CODE",Integer.toString(response.code()));
                            return;
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

                Contact con=null;
                for(Account a : Data.accounts) {
                    if (a.getUsername().equals(LoginActivity.sharedPreferences.getString("display", ""))) {
                        for (Contact c : a.getContacts()) {
                            if(contact.getId() == c.getId()) {
                                con = c;
                                break;
                            }
                        }
                    }
                }

                if(con != null){
                    Data.contacts.remove(con);
                    for(Account a : Data.accounts){
                        a.getContacts().remove(con);
                    }
                }



                Intent intent = new Intent(this,ContactsActivity.class);
                startActivity(intent);
                return true;


        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void pickImageFromGallery(){
        Intent in = new Intent(Intent.ACTION_PICK);
        in.setType("image/*");
        startActivityForResult(in, IMAGE_PICK_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSION_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //permission granted
                    pickImageFromGallery();
                }
                else{
                    //denied
                    Toast.makeText(this,"Permission denied...",Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE && data!=null){
            contactImage.setImageURI(data.getData());


            photoPath = data.getData().toString();

        }
    }
}
