package com.example.email;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.model.Account;
import com.example.email.model.Condition;
import com.example.email.model.Contact;
import com.example.email.model.Folder;
import com.example.email.model.Message;
import com.example.email.model.Operation;
import com.example.email.model.Rule;

import java.util.ArrayList;
import java.util.List;

public class FolderActivity extends AppCompatActivity {

    ListView list;
    List<Message> emails = new ArrayList<>();
    List<Message> zaDrafts = new ArrayList<>();
    int idFolder = -1;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);
        //FoldersActivity.accountFolders();
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("ID");
        idFolder = Integer.parseInt(id);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        managingRules();
        //managingMessagesByRule();


        list = findViewById(R.id.listViewFolder);
        MyAdapter adapter = null;
        if(idFolder != 10){
            adapter = new MyAdapter(this, emails);
        }
        else{
            for(Folder f : FoldersActivity.accountFolders){
                if(f.getId() == 10){
                    zaDrafts = f.getMessages();
                    adapter = new MyAdapter(this, f.getMessages());
                    break;
                }
            }
        }
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                int id = -1;
                if(idFolder != 10){
                    for(int i=0;i<emails.size();i++){
                        if(i == position){
                            id = emails.get(i).getId();
                        }
                    }
                    String s = Integer.toString(id);
                    Intent intent = new Intent(FolderActivity.this,EmailActivity.class);
                    intent.putExtra("ID", s);
                    startActivity(intent);
                }
                else{
                    for(int i=0;i<zaDrafts.size();i++) {
                        if (i == position) {
                            id = zaDrafts.get(i).getId();
                        }
                    }

                    Intent intent = new Intent(FolderActivity.this,UpdateEmailActivity.class);
                    intent.putExtra("ID" , Integer.toString(id));
                    startActivity(intent);
                }

            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        if(idFolder == 1){
            return true;
        }
        else if(idFolder == 2){
            return true;
        }
        else if(idFolder == 10){
            return true;
        }
        else{
            getMenuInflater().inflate(R.menu.folder_menu, menu);
            return true;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    class MyAdapter extends ArrayAdapter<Message>{
        Context context;
        List<Message> emails;
        MyAdapter(Context c, List<Message> emails){
            super(c,R.layout.row_folder,R.id.tekst1, emails);
            this.context=c;
            this.emails = emails;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = li.inflate(R.layout.row_folder, parent, false);
            ImageView images = row.findViewById(R.id.ikonica);
            TextView myTitle = row.findViewById(R.id.tekst1);
            TextView myDesc = row.findViewById(R.id.tekst2);

            String emailTitle = "";
            String emailText = "";
            for(int i =0;i<emails.size();i++){
                if(i == position){
                    emailTitle = emails.get(i).getSubject();
                    emailText = emails.get(i).getContent();
                    break;
                }
            }
            myTitle.setText(emailTitle);
            myDesc.setText(emailText);

            return row;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();


        if(id == R.id.editFolder){
            Intent intent = new Intent(FolderActivity.this, UpdateFolderActivity.class);
            intent.putExtra("ID_FOLDER", Integer.toString(idFolder));
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void managingRules(){
        for(Folder f : FoldersActivity.accountFolders){
            if(id != null){
                if(Integer.parseInt(id) == f.getId()){
                    //Log.i("USLO U IF FOLDERA",Integer.toString(f.getId()));
                    for(Rule r : Data.rules){
                        if(r.getFolder().getId() == f.getId()){
                            //Log.i("CONDITION RULA FOLDERA",r.getCondition().toString()+r.getValue());
                            if(r.getCondition().equals(Condition.FROM) && r.getValue().equals("anyone")){
                                //Log.i("USLO U IF RULEA","ZA SADA");
                                for(Message m : EmailsActivity.accountMessages){
                                    for(String c : m.getTo()){
                                        if(c.equals(LoginActivity.sharedPreferences.getString("display", ""))){
                                            //Log.i("USLO U IF contacta","BBBBB");
                                            emails.add(m);
                                            //f.getRules().add(r);
                                        }
                                    }
                                }

                            }
                            else if(r.getCondition().equals(Condition.TO) && r.getValue().equals("anyone")){
                                for(Message m : EmailsActivity.accountMessages){
                                    if(m.getFrom().equals(LoginActivity.sharedPreferences.getString("display", ""))){
                                        //Log.i("USLO U IF contacta","BBBBB");
                                        emails.add(m);
                                        //f.getRules().add(r);
                                    }
                                }
                            }
                            else{
                                //ako je condition FROM u rulu clicknutog foldera
                                if(r.getCondition().equals(Condition.FROM)){
                                    if(r.getOperation().equals(Operation.MOVE)){
                                        for(Message m : EmailsActivity.accountMessages){
                                            if(m.getFrom().equals(r.getValue())){
                                                //Data.fromMove.remove(m);
                                                //emails.add(m);
                                                emails.add(m);

                                                //f.getRules().add(r);
                                            }
                                        }
                                    }
                                    else if(r.getOperation().equals(Operation.COPY)){
                                        for(Message m : EmailsActivity.accountMessages){
                                            if(m.getFrom().equals(r.getValue())){
                                                emails.add(m);
                                                //f.getRules().add(r);
                                            }
                                        }
                                    }
                                    else if(r.getOperation().equals(Operation.DELETE)){
                                        for(Message m : EmailsActivity.accountMessages){
                                            if(m.getFrom().equals(r.getValue())){
                                                //emails.add(m);
                                                //Data.fromZaBrisanje.add(m);
                                                emails.add(m);

                                                //f.getRules().add(r);
                                            }
                                        }
                                    }
                                }
                                else if(r.getCondition().equals(Condition.TO)){
                                    if(r.getOperation().equals(Operation.MOVE)){
                                        for(Message m :EmailsActivity.accountMessages){
                                            for(String c : m.getTo()){
                                                if(c.equals(r.getValue())){
                                                    //emails.add(m);
                                                    emails.add(m);

                                                    //f.getRules().add(r);
                                                }
                                            }

                                        }
                                    }
                                    else if(r.getOperation().equals(Operation.COPY)){
                                        for(Message m :EmailsActivity.accountMessages){
                                            for(String c : m.getTo()){
                                                if(c.equals(r.getValue())){
                                                    emails.add(m);
                                                    //f.getRules().add(r);
                                                }
                                            }

                                        }
                                    }
                                    else if(r.getOperation().equals(Operation.DELETE)){
                                        for(Message m :EmailsActivity.accountMessages){
                                            for(String c : m.getTo()){
                                                if(c.equals(r.getValue())){
                                                    //emails.add(m);
                                                    emails.add(m);

                                                    //f.getRules().add(r);
                                                }
                                            }

                                        }
                                    }
                                }
                                else if(r.getCondition().equals(Condition.CC)){
                                    if(r.getOperation().equals(Operation.MOVE)){
                                        for(Message m :EmailsActivity.accountMessages){
                                            for(String c : m.getCc()){
                                                if(c.equals(r.getValue())){
                                                    //emails.add(m);
                                                    emails.add(m);

                                                    //f.getRules().add(r);
                                                }
                                            }

                                        }
                                    }
                                    else if(r.getOperation().equals(Operation.COPY)){
                                        for(Message m :EmailsActivity.accountMessages){
                                            for(String c : m.getCc()){
                                                if(c.equals(r.getValue())){
                                                    emails.add(m);
                                                    //f.getRules().add(r);
                                                }
                                            }

                                        }
                                    }
                                    else if(r.getOperation().equals(Operation.DELETE)){
                                        for(Message m :EmailsActivity.accountMessages){
                                            for(String c : m.getCc()){
                                                if(c.equals(r.getValue())){
                                                    emails.add(m);

                                                    //f.getRules().add(r);
                                                }
                                            }

                                        }
                                    }
                                }
                                else if(r.getCondition().equals(Condition.SUBJECT)){
                                    if(r.getOperation().equals(Operation.MOVE)){
                                        for(Message m :EmailsActivity.accountMessages){
                                            if(m.getSubject().equals(r.getValue())){
                                                //emails.add(m);
                                                emails.add(m);

                                                //f.getRules().add(r);
                                            }

                                        }
                                    }
                                    else if(r.getOperation().equals(Operation.COPY)){
                                        for(Message m :EmailsActivity.accountMessages){
                                            if(m.getSubject().equals(r.getValue())){
                                                emails.add(m);
                                                //f.getRules().add(r);
                                            }

                                        }
                                    }
                                    else if(r.getOperation().equals(Operation.DELETE)){
                                        for(Message m :EmailsActivity.accountMessages){
                                            if(m.getSubject().equals(r.getValue())){
                                                //emails.add(m);
                                                emails.add(m);

                                                //f.getRules().add(r);
                                            }

                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }

        }
    }
    /*
    public void managingMessagesByRule(){
        for(Folder f : FoldersActivity.accountFolders){
            if(id != null) {
                if (Integer.parseInt(id) == f.getId()) {
                    for(Rule r : f.getRules()){
                        if(r.getFolder().getId() == f.getId()){
                            if(!emails.isEmpty()){
                                jokerList = emails;
                            }
                            if(r.getCondition().equals(Condition.FROM) && r.getValue().equals("anyone")){
                                jokerList = emails;
                            }
                            if(r.getCondition().equals(Condition.TO) && r.getValue().equals("anyone")){
                                jokerList = emails;
                            }
                            if(r.getCondition().equals(Condition.FROM) && r.getOperation().equals(Operation.COPY)){
                                jokerList = emails;
                            }
                            if(r.getCondition().equals(Condition.FROM) && r.getOperation().equals(Operation.MOVE)){
                                jokerList = fromMove;
                            }
                            if(r.getCondition().equals(Condition.FROM) && r.getOperation().equals(Operation.DELETE)){
                                jokerList = fromDelete;
                            }
                        }
                    }
                }
            }
        }
    }*/
}
