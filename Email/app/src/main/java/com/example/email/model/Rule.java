package com.example.email.model;

public class Rule {

	private int id;
	private Condition condition;
	private Operation operation;
	private String value;
	private Folder folder;

	public Rule(){

	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Operation getOperation() {return operation;}
	public void setOperation(Operation operation) { this.operation = operation;}
	public Folder getFolder() {
		return folder;
	}
	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public Condition getCondition() {
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
