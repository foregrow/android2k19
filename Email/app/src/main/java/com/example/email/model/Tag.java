package com.example.email.model;

import java.util.ArrayList;
import java.util.List;

public class Tag {


	private int id;
	private String name;
	private List<Message> messages = new ArrayList<>();
	private Account account;

	public Tag(){

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}


}
