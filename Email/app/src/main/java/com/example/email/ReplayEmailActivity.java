package com.example.email;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.data.RestService;
import com.example.email.data.Url;
import com.example.email.model.Folder;
import com.example.email.model.Message;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ReplayEmailActivity extends AppCompatActivity {

    TextView twTo;
    TextView twTitle;
    TextView twContent;
    EditText etReply;

    String messageId = "";
    String to;
    String title;
    String content;
    String reply = "";
    String code;

    Retrofit retrofit;
    RestService restService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replay_email);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        code = bundle.getString("CODE");
        if(code.equals("1")){
            to = bundle.getString("FROM");
            title = bundle.getString("TITLE");
            content = bundle.getString("CONTENT");
            messageId = bundle.getString("ID");
        }
        else if(code.equals("2")){
            to = bundle.getString("FROM");
            title = bundle.getString("TITLE");
            content = bundle.getString("CONTENT");
            messageId = bundle.getString("ID");
            for(Message m : Data.emails){
                if(m.getId() == Integer.parseInt(messageId)){
                    for(String s : m.getTo()){
                        if(!LoginActivity.sharedPreferences.getString("display", "").equals(s)){
                            to += "," +s;
                        }

                    }
                }
            }
        }


        twTo = findViewById(R.id.twTo);
        twTitle = findViewById(R.id.twNaslovReply);
        twContent = findViewById(R.id.twSadrzajReply);
        etReply = findViewById(R.id.etReply);

        twTo.setText(to);
        twTitle.setText(title);
        twContent.setText(content);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.reply_email_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        if(code.equals("1")){
            switch (id) {
                case android.R.id.home:
                    reply = etReply.getText().toString().trim();


                    String loggedIn = LoginActivity.sharedPreferences.getString("display", "");

                    java.text.SimpleDateFormat sdf =
                            new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Calendar now = Calendar.getInstance();

                    String date = sdf.format(now.getTime());

                    Message message = new Message();
                    message.setId(hashCode());
                    message.setFrom(loggedIn);
                    message.setSubject(title);
                    message.setContent(reply);
                    message.getTo().add(to);
                    message.setDateTime(date);
                    for(Folder f : FoldersActivity.accountFolders){
                        if(f.getId() == 10){
                            message.setFolder(f);
                            Log.i("FOLDEEEEEEEEEEEEEEER",message.getSubject());
                        }
                    }


                    if(reply.equals("")){
                        reply.equals("-1");
                    }
                    for(Folder f : FoldersActivity.accountFolders){
                        if(f.getId() == 10){
                            f.getMessages().add(message);
                        }
                    }

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Url.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    RestService restService = retrofit.create(RestService.class);

                    Call<Message> call = restService.createMessage(message.getId(), message.getFrom(), to,"-1","-1",message.getDateTime(),title,reply,10);
                    call.enqueue(new Callback<Message>() {
                        @Override
                        public void onResponse(Call<Message> call, Response<Message> response) {
                            if (!response.isSuccessful()){
                                Log.i("STATUS CODE", Integer.toString(response.code()));
                                return;
                            }

                        }

                        @Override
                        public void onFailure(Call<Message> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });


                    FoldersActivity.adapter.notifyDataSetChanged();

                    Toast.makeText(this, "Poruka sacuvana u folder Drafts!", Toast.LENGTH_SHORT).show();
                    Intent iii = new Intent(this,EmailsActivity.class);
                    startActivity(iii);
                    return true;
                case R.id.cancelReply:
                    Intent i = new Intent(this,EmailActivity.class);
                    i.putExtra("ID", messageId);
                    startActivity(i);
                    finish();
                    return true;
                case R.id.sendReply:
                    reply = etReply.getText().toString().trim();

                    if(reply.equals("")){
                        Toast.makeText(this, "Molimo vas da unesete sadrzaj poruke!", Toast.LENGTH_SHORT).show();
                        return true;
                    }


                    retrofit = new Retrofit.Builder()
                            .baseUrl(Url.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    restService = retrofit.create(RestService.class);

                    loggedIn = LoginActivity.sharedPreferences.getString("display", "");

                    java.text.SimpleDateFormat sdff =
                            new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    now = Calendar.getInstance();

                    String datee = sdff.format(now.getTime());
                    Message mm = new Message();
                    mm.setId(hashCode());
                    mm.setFrom(loggedIn);
                    mm.setSubject(title);
                    mm.setContent(reply);
                    mm.setDateTime(datee);
                    mm.getTo().add(to);
                    Data.emails.add(mm);


                    Call<Message> call1 = restService.createMessage(mm.getId(), mm.getFrom(),to,mm.getDateTime(),title,reply);
                    call1.enqueue(new Callback<Message>() {
                        @Override
                        public void onResponse(Call<Message> call, Response<Message> response) {
                            if (!response.isSuccessful()){
                                Log.i("STATUS CODE", Integer.toString(response.code()));
                                return;
                            }

                            Message m = response.body();
                        }

                        @Override
                        public void onFailure(Call<Message> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });

                    Intent inm = new Intent(this,EmailsActivity.class);
                    startActivity(inm);

                    return true;
            }
        }
        else if(code.equals("2")){
            switch (id) {
                case android.R.id.home:
                    reply = etReply.getText().toString().trim();

                    String loggedIn = LoginActivity.sharedPreferences.getString("display", "");

                    java.text.SimpleDateFormat sdf =
                            new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Calendar now = Calendar.getInstance();

                    String date = sdf.format(now.getTime());

                    Message message = new Message();
                    message.setId(hashCode());
                    message.setFrom(loggedIn);
                    message.setSubject(title);
                    message.setContent(reply);
                    message.getTo().add(to);
                    message.setDateTime(date);
                    for(Folder f : FoldersActivity.accountFolders){
                        if(f.getId() == 10){
                            message.setFolder(f);
                            Log.i("FOLDEEEEEEEEEEEEEEER",message.getSubject());
                        }
                    }


                    if(reply.equals("")){
                        reply.equals("-1");
                    }
                    for(Folder f : FoldersActivity.accountFolders){
                        if(f.getId() == 10){
                            f.getMessages().add(message);
                        }
                    }

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Url.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    RestService restService = retrofit.create(RestService.class);

                    Call<Message> call = restService.createMessage(message.getId(), message.getFrom(), to,"-1","-1",message.getDateTime(),title,reply,10);
                    call.enqueue(new Callback<Message>() {
                        @Override
                        public void onResponse(Call<Message> call, Response<Message> response) {
                            if (!response.isSuccessful()){
                                Log.i("STATUS CODE", Integer.toString(response.code()));
                                return;
                            }

                        }

                        @Override
                        public void onFailure(Call<Message> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });


                    FoldersActivity.adapter.notifyDataSetChanged();

                    Toast.makeText(this, "Poruka sacuvana u folder Drafts!", Toast.LENGTH_SHORT).show();
                    Intent iii = new Intent(this,EmailsActivity.class);
                    startActivity(iii);
                    return true;
                case R.id.cancelReply:
                    Intent i = new Intent(this,EmailActivity.class);
                    i.putExtra("ID", messageId);
                    startActivity(i);
                    finish();
                    return true;
                case R.id.sendReply:
                    reply = etReply.getText().toString().trim();

                    if(reply.equals("")){
                        Toast.makeText(this, "Molimo vas da unesete sadrzaj poruke!", Toast.LENGTH_SHORT).show();
                        return true;
                    }


                    retrofit = new Retrofit.Builder()
                            .baseUrl(Url.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    restService = retrofit.create(RestService.class);

                    loggedIn = LoginActivity.sharedPreferences.getString("display", "");

                    java.text.SimpleDateFormat sdff =
                            new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    now = Calendar.getInstance();

                    String datee = sdff.format(now.getTime());
                    Message mm = new Message();
                    mm.setId(hashCode());
                    mm.setFrom(loggedIn);
                    mm.setSubject(title);
                    mm.setContent(reply);
                    mm.setDateTime(datee);
                    mm.getTo().add(to);
                    Data.emails.add(mm);


                    Call<Message> call1 = restService.createMessage(mm.getId(), mm.getFrom(),to,mm.getDateTime(),title,reply);
                    call1.enqueue(new Callback<Message>() {
                        @Override
                        public void onResponse(Call<Message> call, Response<Message> response) {
                            if (!response.isSuccessful()){
                                Log.i("STATUS CODE", Integer.toString(response.code()));
                                return;
                            }

                            Message m = response.body();
                        }

                        @Override
                        public void onFailure(Call<Message> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });

                    Intent inm = new Intent(this,EmailsActivity.class);
                    startActivity(inm);

                    return true;
            }
        }


        return super.onOptionsItemSelected(item);
    }
}
