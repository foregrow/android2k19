package com.example.email;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.data.RestService;
import com.example.email.data.Url;
import com.example.email.model.Account;
import com.example.email.model.Attachment;
import com.example.email.model.Contact;
import com.example.email.model.Message;
import com.example.email.model.Tag;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmailActivity extends AppCompatActivity {

    ListView list;
    public static List<Attachment> attachments = new ArrayList<>();
    private static int messageId = -1;
    Retrofit retrofit;
    RestService restService;

    private static int idd = -1;

    private static String from = "";
    private static String to = "";
    private static String cc = "";
    private static String bcc = "";
    private static String title = "";
    private static String desc = "";
    private static String tags = "";


    private static TextView senderRe;
    private static TextView twTo;
    private static TextView twCc;
    private static TextView twBcc;
    private static TextView text;
    private static TextView naslov;
    private static TextView twTags;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        attachments.clear();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("ID");
        idd = -1;
        if(id != null){
            idd = Integer.parseInt(id);
        }
        fill();
        twTags = findViewById(R.id.twTags);
        senderRe = findViewById(R.id.messageFrom);
        twTo = findViewById(R.id.messageTo);
        twCc = findViewById(R.id.messageCc);
        twBcc = findViewById(R.id.messageBcc);
        text = findViewById(R.id.textPoruke);
        naslov = findViewById(R.id.naslovPoruke);

        String bezZarezaTo = "";
        if(!to.equals("")){
            bezZarezaTo += to.substring(0, to.length() - 1);
            twTo.setText("Primalac: " + bezZarezaTo);
        }
        else{
            twTo.setVisibility(View.GONE);
        }
        String bezZarezaCc = "";
        if(!cc.equals("")){
            bezZarezaCc += cc.substring(0, cc.length() - 1);
            twCc.setText("CC: " + bezZarezaCc);
        }
        else{
            twCc.setVisibility(View.GONE);
        }

        if(!managingBcc()){
            twBcc.setVisibility(View.GONE);
        }

        twTags.setText(tags);
        senderRe.setText("Posiljalac: " + from);
        naslov.setText(title);
        text.setText(desc);




        list = findViewById(R.id.listViewAttachments);
        MyAdapterAttachments adapter = new MyAdapterAttachments(this, attachments);
        list.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.email_menu, menu);
        return true;
    }

    class MyAdapterAttachments extends ArrayAdapter<Attachment> {
        Context context;
        List<Attachment> attachments;
        MyAdapterAttachments(Context c, List<Attachment> attachments){
            super(c,R.layout.row_attachments,R.id.tekst1, attachments);
            this.context=c;
            this.attachments = attachments;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = li.inflate(R.layout.row_attachments, parent, false);
            ImageView images = row.findViewById(R.id.ikonica);
            TextView myname = row.findViewById(R.id.tekst1);
            TextView mytype = row.findViewById(R.id.tekst2);

            String name = "";
            String type = "";
            for(int i =0;i<attachments.size();i++){
                if(i == position){
                    name = attachments.get(i).getName();
                    type = attachments.get(i).getType();
                    break;
                }
            }
            myname.setText(name);
            mytype.setText(type);

            return row;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();
        to = "";
        cc = "";
        bcc = "";
        tags = "";
        from = "";
        title = "";
        desc = "";

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        if(id == R.id.deleteEmail){
            retrofit = new Retrofit.Builder()
                    .baseUrl(Url.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            restService = retrofit.create(RestService.class);
            Call<Void> callv = restService.deleteMessage(messageId);
            callv.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if(!response.isSuccessful()){
                        Log.i("RESPONSE CODE",Integer.toString(response.code()));
                        return;
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    t.printStackTrace();
                }
            });

            Message mess=null;
            for(Account a : Data.accounts) {
                if (a.getUsername().equals(LoginActivity.sharedPreferences.getString("display", ""))) {
                    for (Message m : a.getMessages()) {
                        if(messageId == m.getId()) {
                            mess = m;
                            break;
                        }
                    }
                }
            }

            if(mess != null){
                Log.i("MESS","NIJE NULL");
                for(Account a : Data.accounts){
                    if(a.getUsername().equals(LoginActivity.sharedPreferences.getString("display", ""))){
                        a.getMessages().remove(mess);
                        EmailsActivity.accountMessages = a.getMessages();
                        break;
                    }
                }
            }

            EmailsActivity.adapter.notifyDataSetChanged();
            Toast.makeText(this, "Poruka obrisana!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,EmailsActivity.class);
            if(mess != null){
                intent.putExtra("MESS",mess.getId());
            }
            startActivity(intent);
        }
        if(id == R.id.replyEmail){
            Intent intent = new Intent(this, ReplayEmailActivity.class);
            intent.putExtra("FROM",from);
            intent.putExtra("TITLE",title);
            intent.putExtra("CONTENT",desc);
            intent.putExtra("ID",Integer.toString(messageId));
            intent.putExtra("CODE","1");
            startActivity(intent);
            finish();

        }
        if(id == R.id.replytoallEmail){
            Intent intent = new Intent(this, ReplayEmailActivity.class);
            intent.putExtra("FROM",from);
            intent.putExtra("TITLE",title);
            intent.putExtra("CONTENT",desc);
            intent.putExtra("ID",Integer.toString(messageId));
            intent.putExtra("CODE","2");
            startActivity(intent);
            finish();
        }
        if(id == R.id.forwardEmail){
            Intent intent = new Intent(this, CreateEmailActivity.class);
            intent.putExtra("TITLE",title);
            intent.putExtra("CONTENT",desc);
            intent.putExtra("CODE","3");
            intent.putExtra("ID",Integer.toString(messageId));
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private static void fill(){

        for(Message m : EmailsActivity.accountMessages){
            if(m.getId() == idd){
                messageId = idd;
                title = m.getSubject();
                desc = m.getContent();
                from = m.getFrom();

                for(Tag t : m.getTags()){
                    if(t.getAccount().getUsername().equals(LoginActivity.sharedPreferences.getString("display", ""))){
                        tags += "#" + t.getName() + " ";
                    }
                }
                for(String s : m.getTo()){
                    to += s + ",";
                }
                for(String s : m.getCc()){
                    cc += s + ",";
                }
                for(Attachment a : Data.attachments){
                    if(a.getMessage() != null){
                        if(a.getMessage().getId() == m.getId()){
                            m.getAttachments().add(a);
                            attachments = m.getAttachments();
                            break;
                        }
                    }

                }
                break;
            }
        }

    }

    private static boolean managingBcc(){
        String bezZarezaBcc = "";
        for(Message m : EmailsActivity.accountMessages){
            if(m.getId() == messageId){
                if(m.getFrom().equals(LoginActivity.sharedPreferences.getString("display", ""))){
                    for(String s : m.getBcc()){
                        bcc += s + ",";
                    }
                    bezZarezaBcc += bcc.substring(0, bcc.length() - 1);
                    twBcc.setText("BCC: " + bezZarezaBcc);
                    return true;
                }
                if(m.getBcc().contains(LoginActivity.sharedPreferences.getString("display", ""))){
                    bcc = LoginActivity.sharedPreferences.getString("display", "");
                    twBcc.setText("BCC: " + bcc);
                    return true;
                }
            }
        }

        return false;
    }
}
