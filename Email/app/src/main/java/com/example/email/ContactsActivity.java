package com.example.email;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.model.Account;
import com.example.email.model.Contact;
import com.example.email.model.Folder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ContactsActivity extends AppCompatActivity {

    private DrawerLayout mLinearLayout;
    ListView list;

    public static MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //MainActivity.RetrofitContacts();
        setContentView(R.layout.activity_contacts);

        //accountContacts.clear();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);


        mLinearLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        View headerview = navigationView.getHeaderView(0);
        ImageView txtProfile = headerview.findViewById(R.id.imgProfile);

        txtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactsActivity.this,ProfileActivity.class);
                startActivity(intent);
            }
        });




        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                //int id = menuItem.getItemId();
                String title = menuItem.getTitle().toString();
                Intent intent;
                if(title.equals("Mejlovi")){
                    intent = new Intent(ContactsActivity.this,EmailsActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Folderi")){
                    intent = new Intent(ContactsActivity.this,FoldersActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Podesavanja")){
                    intent = new Intent(ContactsActivity.this,SettingsActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Odjava")){
                    LoginActivity.editor.remove("display").commit();
                    Log.i("OBRISAN",LoginActivity.sharedPreferences.getAll().toString());
                    Intent intentL = new Intent(getApplicationContext(), LoginActivity.class);
                    intentL.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentL);
                }
                mLinearLayout.closeDrawers();
                return true;

            }
        });

        FloatingActionButton fab = findViewById(R.id.floatingActionBarContacts);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent in = new Intent(ContactsActivity.this,CreateContactActivity.class);
                startActivity(in);
            }
        });



        list = findViewById(R.id.listViewContacts);

        adapter = new MyAdapter(this, EmailsActivity.accountContacts);
        list.setAdapter(adapter);
        //adapter.notifyDataSetChanged();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                            int id = -1;
                            for(int i=0;i<EmailsActivity.accountContacts.size();i++){
                                if(i == position){
                                    id = EmailsActivity.accountContacts.get(i).getId();
                                    break;
                    }
                }
                Intent intent = new Intent(ContactsActivity.this,ContactActivity.class);
                String s = Integer.toString(id);
                intent.putExtra("ID", s);
                startActivity(intent);
            }
        });

    }

    class MyAdapter extends ArrayAdapter<Contact> {
        Context context;
        List<Contact> contacts;
        MyAdapter(Context c, List<Contact> contacts){
            super(c,R.layout.row_contacts,R.id.tekst1, EmailsActivity.accountContacts);
            this.context=c;
            this.contacts = contacts;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = li.inflate(R.layout.row_contacts, parent, false);
            ImageView images = row.findViewById(R.id.contactsIkonica);
            TextView myFirst = row.findViewById(R.id.tekst1);
            TextView myLast = row.findViewById(R.id.tekst2);
            String first = "";
            String last = "";
            String path = "";
            for(int i =0;i<contacts.size();i++){
                if(i == position){
                    if(contacts.get(i).getPhoto() == null){
                        first = contacts.get(i).getFirstName();
                        last = contacts.get(i).getLastName();
                        continue;
                    }
                    else{
                        first = contacts.get(i).getFirstName();
                        last = contacts.get(i).getLastName();
                        path = contacts.get(i).getPhoto().getPath();
                    }
                }
            }
            Uri myUri = Uri.parse(path);
            myFirst.setText(first);
            myLast.setText(last);

            if(!myUri.equals("") && myUri != null){
                images.setImageURI(myUri);
            }


           // Picasso.with(context).load(path).into(images);
            return row;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.contacts_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mLinearLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.createContact:
                Intent in = new Intent(ContactsActivity.this,CreateContactActivity.class);
                startActivity(in);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //MainActivity.RetrofitContacts();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
