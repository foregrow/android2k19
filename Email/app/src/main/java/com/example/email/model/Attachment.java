package com.example.email.model;

import android.util.Base64;

public class Attachment {


	private int id;
	private String data;
	private String type;
	private String name;
	private Message message;

	public Attachment(){

	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Message getMessage(){return message;}

	public void setMessage(Message message) {this.message = message;}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
