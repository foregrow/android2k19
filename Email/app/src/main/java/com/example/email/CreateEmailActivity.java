package com.example.email;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.data.RestService;
import com.example.email.data.Url;
import com.example.email.model.Account;
import com.example.email.model.Attachment;
import com.example.email.model.Contact;
import com.example.email.model.Folder;
import com.example.email.model.Message;
import com.example.email.model.Tag;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateEmailActivity extends AppCompatActivity {

    EditText etTags;
    EditText etnaslov;
    MultiAutoCompleteTextView ettoo;
    MultiAutoCompleteTextView etcc;
    MultiAutoCompleteTextView etbcc;
    EditText etporuka;
    String naslov = "";
    String too = "";
    String cc = "";
    String bcc = "";
    String poruka = "";

    String tagId = "";
    String tagName = "";
    String code = "";
    String id = "";

    String attachIds = "";
    String attachNames = "";
    String attachData = "";

    private static List<String> EMAILS = new ArrayList<>();
    private static final int REQUEST_CODE = 0;

    private static List<Attachment> attachments = new ArrayList<>();
    ListView lwAttachments;
    private static ArrayAdapter<String> adapter = null;

    String uriString;
    String path;


    String fileName = null;
    String fileExtension = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_email);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        punjenje();

        Bundle bundle = getIntent().getExtras();
        code = bundle.getString("CODE");


        lwAttachments = findViewById(R.id.lwAttachments);
        etnaslov = findViewById(R.id.editTextNaslov);
        ettoo = findViewById(R.id.editTextTO);
        etcc = findViewById(R.id.editTextCC);
        etbcc = findViewById(R.id.editTextBCC);
        etporuka = findViewById(R.id.editTextSadrzaj);
        etTags = findViewById(R.id.editTextTags);




        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, EMAILS);
        ettoo.setAdapter(adapter);
        ettoo.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        etcc.setAdapter(adapter);
        etcc.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        etbcc.setAdapter(adapter);
        etbcc.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());


        if(code.equals("3")){
            Log.i("CODEEEEEEE",code);
            naslov = bundle.getString("TITLE");
            poruka = bundle.getString("CONTENT");
            etnaslov.setText(naslov);
            etporuka.setText(poruka);
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_email_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                naslov = etnaslov.getText().toString().trim();
                too = ettoo.getText().toString().trim();
                cc = etcc.getText().toString().trim();
                bcc = etbcc.getText().toString().trim();
                poruka = etporuka.getText().toString().trim();
                if(!naslov.equals("") || !too.equals("") || !cc.equals("") || !bcc.equals("") || !poruka.equals("")){

                    String loggedIn = LoginActivity.sharedPreferences.getString("display", "");

                    java.text.SimpleDateFormat sdf =
                            new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Calendar now = Calendar.getInstance();

                    String date = sdf.format(now.getTime());

                    Message message = new Message();
                    message.setId(hashCode());
                    message.setFrom(loggedIn);
                    message.setSubject(naslov);
                    message.setContent(poruka);
                    message.setDateTime(date);
                    for(Folder f : FoldersActivity.accountFolders){
                        if(f.getId() == 10){
                            message.setFolder(f);
                            Log.i("FOLDEEEEEEEEEEEEEEER",message.getSubject());
                        }
                    }
                    String[] toInputs = null;

                    if(!too.equals("")){
                        toInputs = too.split("\\s*,\\s*");
                    }
                    String[] ccInputs = null;
                    if(!cc.equals("")){
                        ccInputs = cc.split("\\s*,\\s*");
                    }
                    String[] bccInputs = null;
                    if(!bcc.equals("")){
                        bccInputs = bcc.split("\\s*,\\s*");
                    }
                    Data.emails.add(message);

                    for(Message m : Data.emails){
                        if(m.getId() == message.getId()){
                            if(toInputs != null) {
                                for (String s : toInputs) {
                                    message.getTo().add(s);
                                }
                            }
                            if(ccInputs != null){
                                for(String s : ccInputs){
                                    message.getCc().add(s);
                                }
                            }
                            if(bccInputs != null){
                                for(String s : bccInputs){
                                    message.getBcc().add(s);
                                }
                            }
                            break;
                        }
                    }
                    if(too.equals("")){
                        too = "-1";
                    }
                    if(cc.equals("")){
                        cc = "-1";
                    }
                    if(bcc.equals("")){
                        bcc = "-1";
                    }
                    if(naslov.equals("")){
                        naslov = "-1";
                    }
                    if(poruka.equals("")){
                        poruka.equals("-1");
                    }
                    for(Folder f : FoldersActivity.accountFolders){
                        if(f.getId() == 10){
                            f.getMessages().add(message);
                        }
                    }

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Url.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    RestService restService = retrofit.create(RestService.class);

                    Call<Message> call = restService.createMessage(message.getId(), message.getFrom(), too,cc,bcc,message.getDateTime(),naslov,poruka,10);
                    call.enqueue(new Callback<Message>() {
                        @Override
                        public void onResponse(Call<Message> call, Response<Message> response) {
                            if (!response.isSuccessful()){
                                Log.i("STATUS CODE", Integer.toString(response.code()));
                                return;
                            }

                        }

                        @Override
                        public void onFailure(Call<Message> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });


                    FoldersActivity.adapter.notifyDataSetChanged();


                    Toast.makeText(this, "Poruka sacuvana u folder Drafts!", Toast.LENGTH_SHORT).show();
                }

                Intent iii = new Intent(this,EmailsActivity.class);
                startActivity(iii);
                return true;

            case R.id.cancelEmail:
                Intent i = new Intent(this,EmailsActivity.class);
                startActivity(i);

                /*if(!attachments.isEmpty()){
                    for(int j = 0 ; j<attachments.size();j++){
                        attachments.get(j).setId(j);
                        attachIds += Integer.toString(attachments.get(j).getId()) + ",";
                        attachNames += attachments.get(j).getName() + ",";
                        attachData += attachments.get(j).getData() + ",";
                    }
                    Log.i("IDS",attachIds);
                    Log.i("NAMES",attachNames);
                    Log.i("DATA",attachData);
                }*/
                return true;

            case R.id.sendEmail:
                naslov = etnaslov.getText().toString().trim();
                too = ettoo.getText().toString().trim();
                cc = etcc.getText().toString().trim();
                bcc = etbcc.getText().toString().trim();
                poruka = etporuka.getText().toString().trim();
                tagName = etTags.getText().toString().trim();

                //Toast.makeText(this,too, Toast.LENGTH_SHORT).show();

                if(naslov.equals("")){
                    Toast.makeText(this, "Polje naslov ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(too.equals("")){
                    Toast.makeText(this, "Polje TO ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(poruka.equals("")){
                    Toast.makeText(this, "Polje sadrzaj ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                RestService restService = retrofit.create(RestService.class);

                String loggedIn = LoginActivity.sharedPreferences.getString("display", "");


                String[] toInputs = too.split("\\s*,\\s*");
                String[] ccInputs = null;
                if(!cc.equals("")){
                    ccInputs = cc.split("\\s*,\\s*");
                }
                String[] bccInputs = null;
                if(!bcc.equals("")){
                    bccInputs = bcc.split("\\s*,\\s*");
                }

                java.text.SimpleDateFormat sdf =
                        new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Calendar now = Calendar.getInstance();

                String date = sdf.format(now.getTime());
                Message message = new Message();
                message.setId(hashCode());
                message.setFrom(loggedIn);
                message.setSubject(naslov);
                message.setContent(poruka);
                message.setDateTime(date);
                Data.emails.add(message);

                if(!attachments.isEmpty()){
                    for(int j = 0 ; j<attachments.size();j++){
                        attachments.get(j).setId(j);
                        attachIds += Integer.toString(attachments.get(j).getId()) + ",";
                        attachNames += attachments.get(j).getName() + ",";
                        attachData += attachments.get(j).getData() + ",";
                        attachments.get(j).setMessage(message);
                    }
                }

                for(Message m : Data.emails){
                    if(m.getId() == message.getId()){
                        for(String s : toInputs){
                            message.getTo().add(s);
                        }
                        if(ccInputs != null){
                            for(String s : ccInputs){
                                message.getCc().add(s);
                            }
                        }
                        if(bccInputs != null){
                            for(String s : bccInputs){
                                message.getBcc().add(s);
                            }
                        }
                        break;
                    }
                }
                int broj = hashCode();
                String[] tagsInputs;
                if(!tagName.equals("")){
                    tagsInputs = tagName.split("\\s*,\\s*");
                    for(String s : tagsInputs){
                        Tag t = new Tag();
                        t.setId(broj++);
                        tagId += Integer.toString(t.getId()) + ",";
                        t.setName(s);
                        for(Account a : Data.accounts){
                            if(a.getUsername().equals(loggedIn)){
                                t.setAccount(a);
                            }
                        }
                        message.getTags().add(t);
                        Log.i("IDDDDDDDDDDD",Integer.toString(t.getId()));
                        Log.i("message ID",Integer.toString(message.getId()));
                    }
                }

                if(tagName.equals("")){
                    tagName = "-1";
                    tagId = "-1";
                }
                if(cc.equals("")){
                    cc = "-1";
                }
                if(bcc.equals("")){
                    bcc = "-1";
                }
                Call<Message> call;

                if(attachments.isEmpty()) {
                    call = restService.createMessage(message.getId(), message.getFrom(), too,cc,bcc,message.getDateTime(),naslov,poruka,tagId,tagName,loggedIn);
                }
                else{
                    call = restService.createMessage(message.getId(), message.getFrom(), too,cc,bcc,message.getDateTime(),naslov,poruka,tagId,tagName,loggedIn,attachIds,attachNames,attachData);
                }
                //Call<Message> call = restService.createMessage(message.getId(), message.getSubject(), message.getTo(), message.getCc(), message.getBcc(), message.getDateTime(), message.getContent());

                call.enqueue(new Callback<Message>() {
                    @Override
                    public void onResponse(Call<Message> call, Response<Message> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }

                        Message m = response.body();
                    }

                    @Override
                    public void onFailure(Call<Message> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

                attachments.clear();

                Intent inm = new Intent(this,EmailsActivity.class);
                startActivity(inm);
                return true;
            case R.id.attachFile:
                startSearch();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        MyAdapterAttachments aadapter = new MyAdapterAttachments(this, attachments);
        lwAttachments.setAdapter(aadapter);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void punjenje(){
        String nesto = "";
        for(Contact c : EmailsActivity.accountContacts){
            EMAILS.add(c.getEmail());
        }

    }

    private void startSearch(){

        Intent chooser = new Intent(Intent.ACTION_GET_CONTENT);
        chooser.setType("*/*");
        chooser.addCategory(Intent.CATEGORY_OPENABLE);
        chooser.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        try {
            startActivityForResult(
                    Intent.createChooser(chooser, "Choose a file"),
                    REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK){
            if(data != null){
                Uri uri = data.getData();

                uriString = uri.toString();
                //t2 = uri.getPath();
                File myFile = new File(uriString);
                path = myFile.getAbsolutePath();
                String displayName = null;

                if(uriString.startsWith("content://")){
                    Cursor cursor = null;
                    try{
                        cursor = this.getContentResolver().query(uri, null, null, null, null);
                        if(cursor != null & cursor.moveToFirst()){
                            displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        }
                    } finally{
                        cursor.close();
                    }
                } else if(uriString.startsWith("file://")){
                    displayName = myFile.getName();
                }
                String[] file;
                if(displayName != null){
                    file = displayName.split("\\.");
                    if(file != null){
                        fileName = file[0];
                        fileExtension = file[1];
                        String enc = Base64.encodeToString(path.getBytes(),
                                            Base64.NO_WRAP);
                        Attachment a = new Attachment();
                        a.setName(fileName);
                        a.setType(fileExtension);
                        a.setData(enc);

                        attachments.add(a);

                    }


                    //Log.i("DISPALY NAME",displayName);
                    //tw1.setText(fileName);
                    //tw2.setText(fileExtension);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    class MyAdapterAttachments extends ArrayAdapter<Attachment> {
        Context context;
        List<Attachment> attachments;
        MyAdapterAttachments(Context c, List<Attachment> attachments){
            super(c,R.layout.row_attachments,R.id.tekst1, attachments);
            this.context=c;
            this.attachments = attachments;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = li.inflate(R.layout.row_attachments, parent, false);
            ImageView images = row.findViewById(R.id.ikonica);
            TextView myname = row.findViewById(R.id.tekst1);
            TextView mytype = row.findViewById(R.id.tekst2);

            String name = "";
            String type = "";
            for(int i =0;i<attachments.size();i++){
                if(i == position){
                    name = attachments.get(i).getName();
                    type = attachments.get(i).getType();
                    break;
                }
            }
            myname.setText(name);
            mytype.setText(type);

            return row;
        }
    }
}
