package com.example.email.model;

import java.util.ArrayList;
import java.util.List;

public class Account {


	private int id;
	private String smtp;
	private String pop3;
	private String username;
	private String password;
	private List<Message> messages = new ArrayList<>();
	private List<Contact> contacts = new ArrayList<>();
	private List<Folder> folders = new ArrayList<>();
	private List<Tag> tags = new ArrayList<>();

	public Account(){

	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSmtp() {
		return smtp;
	}

	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPop3() {
		return pop3;
	}

	public void setPop3(String pop3){ this.pop3 = pop3;}

	public List<Message> getMessages(){return messages;}

	public void setMessages(List<Message> messages){this.messages = messages;}


	public List<Contact> getContacts() {
		return contacts;
	}


	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}


	public List<Folder> getFolders() {
		return folders;
	}


	public void setFolders(List<Folder> folders) {
		this.folders = folders;
	}


	public List<Tag> getTags() {
		return tags;
	}


	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}




}
