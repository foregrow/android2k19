package com.example.email;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.model.Account;

public class ChangeProfileActivity extends AppCompatActivity {

    String username;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_profile);
        btnStartEmalsActivity();
    }

    private void btnStartEmalsActivity(){
        Button btnLogin = findViewById(R.id.btnChangeProfile);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText etUsername = findViewById(R.id.etUsername);
                username = etUsername.getText().toString();
                EditText etPassword = findViewById(R.id.etPassword);
                password = etPassword.getText().toString();

                if(Login()){

                    Intent intentL = new Intent(getApplicationContext(), EmailsActivity.class);
                    intentL.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentL);
                }
            }
        });

    }

    private boolean Login(){
        boolean tacno = false;

        for(Account a : Data.accounts){
            if(username.equals(a.getUsername()) && password.equals(a.getPassword())){
                LoginActivity.editor.remove("display").commit();
                Log.i("OBRISAN",LoginActivity.sharedPreferences.getAll().toString());

                LoginActivity.sharedPreferences = getSharedPreferences("MYPREF",MODE_PRIVATE);
                LoginActivity.userDetails = LoginActivity.sharedPreferences.getString("user", a.getUsername());
                LoginActivity.editor = LoginActivity.sharedPreferences.edit();
                LoginActivity.editor.putString("display",LoginActivity.userDetails);
                LoginActivity.editor.commit();
                tacno = true;
                break;
            }
        }

        if(!tacno){
            if(username.equals("") && password.equals("")){
                Toast.makeText(ChangeProfileActivity.this,"Morate uneti sve podatke! ", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(ChangeProfileActivity.this,"Netacni podaci! ", Toast.LENGTH_LONG).show();
            }
        }
        return tacno;
    }
}
