package com.example.email.model;

public enum Condition {

    TO,
    FROM,
    CC,
    SUBJECT
}
