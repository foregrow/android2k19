package com.example.email.data;


import com.example.email.model.Account;
import com.example.email.model.Attachment;
import com.example.email.model.Contact;
import com.example.email.model.Folder;
import com.example.email.model.Message;
import com.example.email.model.Photo;
import com.example.email.model.Rule;

import java.util.ArrayList;
import java.util.List;

public class Data {
    public static List<Message> emails = new ArrayList<>();
    public static List<Contact> contacts = new ArrayList<>();
    public static List<Account> accounts = new ArrayList<>();
    public static List<Folder> folders = new ArrayList<>();
    public static List<Attachment> attachments = new ArrayList<>();
    public static List<Rule> rules = new ArrayList<>();
    public static List<Photo> photos = new ArrayList<>();

    public static void data(){
        Message m1 = new Message();
        m1.setId(1);
        m1.setSubject("Android programiranje");
        m1.setContent("Postovani, Potrebno je dovrsiti implementaciju zadatka do srede 15.4.2019. godine. Pozdrav. ");

        Message m2 = new Message();
        m2.setId(2);
        m2.setSubject("Web programiranje");
        m2.setContent("Postovani, Potrebno je dovrsiti implementaciju zadatka do srede 15.4.2019. godine. Pozdrav. ");

        Message m3 = new Message();
        m3.setId(3);
        m3.setSubject("Rest servisi");
        m3.setContent("Postovani, Potrebno je dovrsiti implementaciju zadatka do srede 15.4.2019. godine. Pozdrav. ");

        Message m4 = new Message();
        m4.setId(4);
        m4.setSubject("Pinterest");
        m4.setContent("Check our lastest photos.. ");

        Message m5 = new Message();
        m5.setId(5);
        m5.setSubject("Facebook");
        m5.setContent("Secure your account.. ");

        Attachment a1 = new Attachment();
        a1.setId(1);
        a1.setName("photo.png");
        a1.setType("png");
        a1.setMessage(m5);
        Attachment a2 = new Attachment();
        a2.setId(2);
        a2.setName("video.mkv");
        a2.setType("mkv");
        a2.setMessage(m5);
        Attachment a3 = new Attachment();
        a3.setId(3);
        a3.setName("audio3.mp3");
        a3.setType("mp3");
        a3.setMessage(m5);
        List<Attachment> attachments = new ArrayList<>();
        attachments.add(a1);
        attachments.add(a2);
        attachments.add(a3);
        m5.setAttachments(attachments);
        m4.setAttachments(attachments);
        m1.getAttachments().add(a1);

        emails.add(m1);
        emails.add(m2);
        emails.add(m3);
        emails.add(m4);
        emails.add(m5);

        Folder f1 = new Folder();
        f1.setId(1);
        f1.setName("Sve poruke");
        f1.setMessages(emails);

        Folder f2 = new Folder();
        f2.setId(2);
        f2.setName("Primljene poruke");
        f2.getMessages().add(m1);
        f2.getMessages().add(m4);
        f2.getMessages().add(m5);

        Folder f3 = new Folder();
        f3.setId(3);
        f3.setName("Poslate poruke");
        f3.getMessages().add(m2);
        f3.getMessages().add(m3);

        folders.add(f1);
        folders.add(f2);
        folders.add(f3);

        Contact c1 = new Contact();
        c1.setId(1);
        c1.setFirstName("Mike");
        c1.setLastName("Ehrmantraut");
        c1.setEmail("mike@ghostmail.com");
        c1.setDisplay("display");

        Contact c2 = new Contact();
        c2.setId(2);
        c2.setFirstName("Gustavo");
        c2.setLastName("Fring");
        c2.setEmail("gus@ghostmail.com");
        c2.setDisplay("display");

        Contact c3 = new Contact();
        c3.setId(3);
        c3.setFirstName("Jimmy");
        c3.setLastName("McGill");
        c3.setEmail("jimmy@ghostmail.com");
        c3.setDisplay("display");

        Contact c4 = new Contact();
        c4.setId(4);
        c4.setFirstName("Walter");
        c4.setLastName("White");
        c4.setEmail("ww@ghostmail.com");
        c4.setDisplay("display");

        contacts.add(c1);
        contacts.add(c2);
        contacts.add(c3);
        contacts.add(c4);
    }

    public static void bla(){

        for(Message m : emails){
            if(m.getId() == 1){
                Attachment a1 = new Attachment();
                a1.setId(1);
                a1.setName("photo.png");
                a1.setType("png");
                a1.setMessage(m);
                Attachment a2 = new Attachment();
                a2.setId(2);
                a2.setName("video.mkv");
                a2.setType("mkv");
                a2.setMessage(m);
                attachments.add(a1);
                attachments.add(a2);
            }
        }
    }
}
