package com.example.email;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.model.Folder;
import com.example.email.model.Message;

import java.util.ArrayList;
import java.util.List;

public class FoldersActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    ListView list;
    public static List<Folder> accountFolders = new ArrayList<>();
    public static MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_folders);
        accountFolders.clear();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view_folders);
        View headerview = navigationView.getHeaderView(0);
        ImageView txtProfile = headerview.findViewById(R.id.imgProfile);

        txtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FoldersActivity.this,ProfileActivity.class);
                startActivity(intent);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                String title = menuItem.getTitle().toString();
                Intent intent;
                if(title.equals("Kontakti")){
                    intent = new Intent(FoldersActivity.this,ContactsActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Mejlovi")){
                    intent = new Intent(FoldersActivity.this,EmailsActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Podesavanja")){
                    intent = new Intent(FoldersActivity.this,SettingsActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Odjava")){
                    LoginActivity.editor.remove("display").commit();
                    Log.i("OBRISAN",LoginActivity.sharedPreferences.getAll().toString());
                    Intent intentL = new Intent(getApplicationContext(), LoginActivity.class);
                    intentL.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentL);
                }
                mDrawerLayout.closeDrawers();
                return true;

            }
        });


        FloatingActionButton fab = findViewById(R.id.floatingActionBarFolders);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent in = new Intent(FoldersActivity.this,CreateFolderActivity.class);
                startActivity(in);
            }
        });


        list = findViewById(R.id.listViewFolders);
        accountFolders();

        adapter = new MyAdapter(this, accountFolders);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Intent intent = new Intent(FoldersActivity.this,FolderActivity.class);

                int id = -1;
                for(int i =0;i<Data.folders.size();i++){
                    if(i == position){
                        id = Data.folders.get(i).getId();
                        break;
                    }
                }
                String s = Integer.toString(id);
                intent.putExtra("ID", s);
                startActivity(intent);
            }
        });


    }

    class MyAdapter extends ArrayAdapter<Folder> {
        Context context;
        List<Folder> folders;
        MyAdapter(Context c, List<Folder> folders){
            super(c,R.layout.row_folders,R.id.tekst1, accountFolders);
            this.context=c;
            this.folders = folders;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = li.inflate(R.layout.row_folders, parent, false);
            ImageView images = row.findViewById(R.id.ikonica);
            TextView myTitle = row.findViewById(R.id.tekst1);
            String folderName = "";
            for(int i =0;i<folders.size();i++){
                if(i == position){
                    folderName = folders.get(i).getName();
                    break;
                }
            }
            myTitle.setText(folderName);
            return row;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.folders_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.createFolder:
                Intent intent = new Intent(FoldersActivity.this, CreateFolderActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //MainActivity.RetrofitFolders();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static void accountFolders(){
        for(Folder f : Data.folders){
            if(f.getName().equals("Primljene")){
                accountFolders.add(f);
            }

            if(f.getName().equals("Drafts")){
                accountFolders.add(f);
            }

            if(f.getName().equals("Poslate")){
                accountFolders.add(f);
            }

            if(f.getAccount() != null){
                if(LoginActivity.sharedPreferences.getString("display", "").equals(f.getAccount().getUsername())){
                    Log.i("REDOM FOLDERI",f.getName());
                    accountFolders.add(f);
                }
            }

        }
    }
}
