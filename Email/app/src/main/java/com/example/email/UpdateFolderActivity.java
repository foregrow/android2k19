package com.example.email;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.data.RestService;
import com.example.email.data.Url;
import com.example.email.model.Condition;
import com.example.email.model.Folder;
import com.example.email.model.Operation;
import com.example.email.model.Rule;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpdateFolderActivity extends AppCompatActivity {

    EditText etName;
    EditText etCondition;
    EditText etOperation;
    EditText etRuleValue;
    int id;
    String name;
    String condition;
    String opertaion;
    String ruleValue;

    Folder folder;
    Retrofit retrofit;
    RestService restService;
    int ruleId = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_folder);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView name = findViewById(R.id.UpdateFolder);
        etCondition = findViewById(R.id.etUCondition);
        etOperation = findViewById(R.id.etUOperation);
        etRuleValue = findViewById(R.id.etURuleValue);

        Bundle bundle = getIntent().getExtras();
        String id = bundle.getString("ID_FOLDER");
        int idd = Integer.parseInt(id);
        for(Folder c : Data.folders){
            if(c.getId() == idd){
                folder = c;
                name.setText(c.getName());
                for(Rule r : c.getRules()){
                    ruleId = r.getId();
                    etCondition.setText(r.getCondition().toString());
                    etOperation.setText(r.getOperation().toString());
                    etRuleValue.setText(r.getValue());
                    break;
                }
            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.update_folder_menu, menu);
        return true;

    }


    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.cancelCreateFolder:
                Intent i = new Intent(this,FoldersActivity.class);
                startActivity(i);
                return true;
            case R.id.UpdateSave:
                etName = findViewById(R.id.UpdateFolder);

                name = etName.getText().toString();
                condition = etCondition.getText().toString();
                opertaion = etOperation.getText().toString();
                ruleValue = etRuleValue.getText().toString();
                List<String> conditions = new ArrayList<>();
                conditions.add("FROM");
                conditions.add("TO");
                conditions.add("CC");
                conditions.add("SUBJECT");
                List<String> operations = new ArrayList<>();
                operations.add("MOVE");
                operations.add("COPY");
                operations.add("DELETE");
                if(name.equals("")){
                    Toast.makeText(this, "Ime foldera ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(!conditions.contains(condition)){
                    Toast.makeText(this, "Condition mora biti FROM, TO, CC ili SUBJECT!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(!operations.contains(opertaion)){
                    Toast.makeText(this, "Operation mora biti MOVE, COPY ili DELETE!", Toast.LENGTH_SHORT).show();
                    return true;
                }

                retrofit = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                restService = retrofit.create(RestService.class);

                Call<Folder> call = restService.updateFolder(folder.getId(),name,ruleId,condition,opertaion,ruleValue);

                call.enqueue(new Callback<Folder>() {
                    @Override
                    public void onResponse(Call<Folder> call, Response<Folder> response) {
                        if(!response.isSuccessful()){
                            Log.i("RESPONSE CODE", Integer.toString(response.code()));
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<Folder> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
                for(Folder f : Data.folders){
                    if(folder.getId() == f.getId()){
                        f.setName(name);
                        for(Rule r : f.getRules()){
                            if(r.getId() == ruleId){
                                r.setCondition(Condition.valueOf(condition));
                                r.setOperation(Operation.valueOf(opertaion));
                                r.setValue(ruleValue);
                                break;
                            }
                        }
                    }
                }
                Intent in = new Intent(this,FoldersActivity.class);
                startActivity(in);
                return true;
            case R.id.DeleteFolder:
                retrofit = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                restService = retrofit.create(RestService.class);
                Call<Void> callv = restService.deleteFolder(folder.getId());
                callv.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if(!response.isSuccessful()){
                            Log.i("RESPONSE CODE",Integer.toString(response.code()));
                            return;
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
                Folder fol=null;
                for(Folder f : Data.folders){
                    if(folder.getId() == f.getId()){
                        fol = f;
                    }
                }
                if(fol != null){
                    Data.folders.remove(fol);
                }
                Intent intent = new Intent(this,FoldersActivity.class);
                startActivity(intent);
                return true;


        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
