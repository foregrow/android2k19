package com.example.email.model;

public enum Operation {
    MOVE,
    COPY,
    DELETE
}
