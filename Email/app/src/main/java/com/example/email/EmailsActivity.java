package com.example.email;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import android.opengl.Visibility;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;

import android.widget.ListView;
import android.widget.TextView;

import com.example.email.data.Data;
import com.example.email.data.RestService;
import com.example.email.data.Url;
import com.example.email.model.Account;
import com.example.email.model.Contact;
import com.example.email.model.Folder;
import com.example.email.model.Message;
import com.example.email.model.Tag;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmailsActivity extends AppCompatActivity {

    TextView myTitle;
    TextView myDesc;
    TextView twSender;
    private DrawerLayout mLinearLayout;
    public static ListView list;
    public static List<Message> accountMessages = new ArrayList<>();
    public static List<Contact> accountContacts = new ArrayList<>();
    public static List<Folder> accountFolders = new ArrayList<>();
    int idPoruke = -1;
    public static MyAdapter adapter;
    TextView twPoruka;

    public static List<Message> novaListaPoruka = new ArrayList<>();




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_emails);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);

        mLinearLayout = findViewById(R.id.drawer_layout);


        NavigationView navigationView = findViewById(R.id.navigation_view);
        View headerview = navigationView.getHeaderView(0);
        ImageView txtProfile = headerview.findViewById(R.id.imgProfile);


        twPoruka = findViewById(R.id.twPoruka);
        twPoruka.setVisibility(View.GONE);

        txtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmailsActivity.this,ProfileActivity.class);
                startActivity(intent);
            }
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                //int id = menuItem.getItemId();
                String title = menuItem.getTitle().toString();
                Intent intent;
                if(title.equals("Kontakti")){
                    intent = new Intent(EmailsActivity.this,ContactsActivity.class);
                    startActivity(intent);

                }
                else if(title.equals("Folderi")){
                    intent = new Intent(EmailsActivity.this,FoldersActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Podesavanja")){
                    intent = new Intent(EmailsActivity.this,SettingsActivity.class);
                    startActivity(intent);
                }
                else if(title.equals("Odjava")){
                    LoginActivity.editor.remove("display").commit();
                    Log.i("OBRISAN",LoginActivity.sharedPreferences.getAll().toString());
                    Intent intentL = new Intent(getApplicationContext(), LoginActivity.class);
                    intentL.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentL);
                }
                mLinearLayout.closeDrawers();
                return true;

            }
        });



        if(LoginActivity.sharedPreferences.contains("display")){
            Log.i("ULOGOVAN",LoginActivity.sharedPreferences.getAll().toString());
        }

        FloatingActionButton fab = findViewById(R.id.floatingActionBar);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent in = new Intent(EmailsActivity.this,CreateEmailActivity.class);
                in.putExtra("CODE","");
                startActivity(in);
            }
        });

        accountMessages.clear();

        MainActivity.RetrofitMessages();
        MainActivity.RetrofitAttachments();

        accountMessages();
        accountContacts();

        list = findViewById(R.id.listViewEmails);

        adapter = new MyAdapter(this, accountMessages);
        list.setAdapter(adapter);



        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                int id = -1;

                for(int i=0;i<accountMessages.size();i++){
                    if(i == position){
                        id = accountMessages.get(i).getId();
                        idPoruke = id;
                        accountMessages.get(i).setChecked(true);
                        checkedTrue();
                    }
                }

                String s = Integer.toString(id);
                Intent intent = new Intent(EmailsActivity.this,EmailActivity.class);
                intent.putExtra("ID", s);
                startActivity(intent);
                adapter.notifyDataSetChanged();
            }
        });


    }


    class MyAdapter extends ArrayAdapter<Message> implements Filterable {
        Context context;
        List<Message> emails;
        List<Message> emailsFull;
        MyAdapter(Context c, List<Message> emails){
            super(c,R.layout.row_emails,R.id.tekst1, emails);
            this.context=c;
            this.emails = emails;
            emailsFull = new ArrayList<>(emails);
        }

        @NonNull
        @Override
        public Filter getFilter() {
            return emailsFilter;
        }
        private Filter emailsFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                List<Message> filteredList = new ArrayList<>();

                if(charSequence == null || charSequence.length() == 0){
                    filteredList.addAll(emailsFull);
                }else{
                    String filterPattern = charSequence.toString().toLowerCase().trim();
                    List<String> tags = new ArrayList<>();
                    List<String> to = new ArrayList<>();
                    List<String> cc = new ArrayList<>();
                    List<String> bcc = new ArrayList<>();
                    for (Message item : emailsFull){
                        for(String s : item.getTo()){
                            to.add(s.toLowerCase());
                        }
                        for(String s : item.getCc()){
                            cc.add(s.toLowerCase());
                        }
                        for(String s : item.getBcc()){
                            bcc.add(s.toLowerCase());
                        }
                        for(Tag t : item.getTags()){
                            tags.add(t.getName().toLowerCase());
                        }
                        if(item.getSubject().toLowerCase().contains(filterPattern) || item.getContent().toLowerCase().contains(filterPattern) ||
                                item.getFrom().toLowerCase().contains(filterPattern) || to.contains(filterPattern) || cc.contains(filterPattern)
                                || bcc.contains(filterPattern) || tags.contains(filterPattern)){
                            filteredList.add(item);
                        }
                    }

                }

                FilterResults results = new FilterResults();
                results.values = filteredList;

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                emails.clear();
                emails.addAll((List) filterResults.values);
                notifyDataSetChanged();

            }
        };

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater li = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = li.inflate(R.layout.row_emails, parent, false);
            ImageView images = row.findViewById(R.id.ikonica);
            myTitle = row.findViewById(R.id.tekst1);
            myDesc = row.findViewById(R.id.tekst2);
            twSender = row.findViewById(R.id.sender);

            String emailTitle = "";
            String emailText = "";
            String sender = "";
            for(int i =0;i<emails.size();i++){
                if(i == position){
                    sender = emails.get(i).getFrom();
                    emailTitle = emails.get(i).getSubject();
                    emailText = emails.get(i).getContent();
                    if(!emails.get(i).isChecked() && !LoginActivity.sharedPreferences.getString("display", "").equals(emails.get(i).getFrom())){
                        myTitle.setTextColor(Color.parseColor("#ff0303"));
                        myDesc.setTextColor(Color.parseColor("#ff0303"));
                        twSender.setTextColor(Color.parseColor("#ff0303"));
                    }

                }
            }


            if(LoginActivity.sharedPreferences.getString("display", "").equals(sender)){
                twSender.setText("You");
            }
            else{
                twSender.setText(sender);
            }
            myTitle.setText(emailTitle);
            myDesc.setText(emailText);

            return row;

        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.emails_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView sw = (SearchView) searchItem.getActionView();

        sw.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                mLinearLayout.openDrawer(GravityCompat.START);
                return true;


        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter != null){
            Log.i("ON RESUME","EMAILSSS");
            adapter.notifyDataSetChanged();
        }

        Bundle bundle = getIntent().getExtras();
        Message mess = null;
        for(Message m : accountMessages){
            if(bundle != null){
                if(m.getId() == bundle.getInt("MESS")){
                    mess = m;
                }
            }
        }
        if(mess != null){
            accountMessages.remove(mess);
        }

        String s = LoginActivity.sharedPreferences.getString("datum","");
        if(s != null && !s.equals("")){
            List<String> dates = new ArrayList<>();
            for(Message m : EmailsActivity.accountMessages){
                dates.add(m.getDateTime());
            }
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

            List<Date> ddates = new ArrayList<>();
            for(String d : dates){
                try{
                    Date f;
                    f = format.parse(d);
                    ddates.add(f);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            if(Integer.parseInt(s) == 0){
                Collections.sort(ddates, Collections.<Date>reverseOrder());



                Date stringToDate;

                for(Date l : ddates){
                    for(Message m : EmailsActivity.accountMessages){
                        try{
                            stringToDate = format.parse(m.getDateTime());
                            if(stringToDate.compareTo(l) ==0 ){
                                novaListaPoruka.add(m);
                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }
                EmailsActivity.accountMessages = novaListaPoruka;

            }else if(Integer.parseInt(s) == 1){
                Collections.sort(ddates);


                Date stringToDate;

                for(Date l : ddates){
                    for(Message m : EmailsActivity.accountMessages){
                        try{
                            stringToDate = format.parse(m.getDateTime());
                            if(stringToDate.compareTo(l) == 0){
                                novaListaPoruka.add(m);
                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                        }
                    }
                }

                EmailsActivity.accountMessages = novaListaPoruka;
            }
        }





    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    public static void accountMessages(){
        for(Account a : Data.accounts){
            if(LoginActivity.sharedPreferences.getString("display", "").equals(a.getUsername())){
                for(Message m : Data.emails){
                    if(m.getFolder() == null) {
                        if (LoginActivity.sharedPreferences.getString("display", "").equals(m.getFrom())) {
                            a.getMessages().add(m);
                        }
                        for (String t : m.getTo()) {
                            if (LoginActivity.sharedPreferences.getString("display", "").equals(t)) {
                                a.getMessages().add(m);
                            }
                        }
                        for (String c : m.getCc()) {
                            if (LoginActivity.sharedPreferences.getString("display", "").equals(c)) {
                                a.getMessages().add(m);
                            }
                        }
                        for (String b : m.getBcc()) {
                            if (LoginActivity.sharedPreferences.getString("display", "").equals(b)) {
                                a.getMessages().add(m);
                            }

                        }
                    }
                }
                accountMessages = a.getMessages();
            }
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    public static void accountContacts(){
        for(Account a : Data.accounts){
            if(a.getUsername().equals(LoginActivity.sharedPreferences.getString("display", ""))){
                accountContacts = a.getContacts();
                return;
            }
        }
    }

    public static void accountFolders(){
        for(Folder f : Data.folders){
            if(f.getName().equals("Primljene")){
                accountFolders.add(f);
            }

            if(f.getName().equals("Drafts")){
                accountFolders.add(f);
            }

            if(f.getName().equals("Poslate")){
                accountFolders.add(f);
            }

            if(f.getAccount() != null){
                if(LoginActivity.sharedPreferences.getString("display", "").equals(f.getAccount().getUsername())){
                    Log.i("REDOM FOLDERI",f.getName());
                    accountFolders.add(f);
                }
            }

        }
    }

    private void checkedTrue(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Url.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestService restService = retrofit.create(RestService.class);
        Call<Message> callv = restService.updateMessage(idPoruke);
        callv.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if(!response.isSuccessful()){
                    Log.i("RESPONSE CODE",Integer.toString(response.code()));
                    return;
                }
            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }





}
