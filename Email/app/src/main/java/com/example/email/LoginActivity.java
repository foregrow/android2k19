package com.example.email;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.SharedPreferences;

import com.example.email.data.Data;
import com.example.email.model.Account;


public class LoginActivity extends AppCompatActivity {

    public static String userDetails;
    String username;
    String password;
    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnStartEmalsActivity();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void btnStartEmalsActivity(){
        Button btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText etUsername = findViewById(R.id.etUsername);
                username = etUsername.getText().toString();
                EditText etPassword = findViewById(R.id.etPassword);
                password = etPassword.getText().toString();

                if(Login()){

                    Intent redirect = new Intent(LoginActivity.this, EmailsActivity.class);
                    startActivity(redirect);
                    finish();
                }

            }
        });
    }

    private boolean Login(){
        boolean tacno = false;

        for(Account a : Data.accounts){
            if(username.equals(a.getUsername()) && password.equals(a.getPassword())){
                sharedPreferences = getSharedPreferences("MYPREF",MODE_PRIVATE);
                userDetails = sharedPreferences.getString("user", a.getUsername());
                editor = sharedPreferences.edit();
                editor.putString("display",userDetails);
                editor.commit();
                tacno = true;
                break;
            }
        }

        if(!tacno){
            if(username.equals("") && password.equals("")){
                Toast.makeText(LoginActivity.this,"Morate uneti sve podatke! ", Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(LoginActivity.this,"Netacni podaci! ", Toast.LENGTH_LONG).show();
            }
        }
        return tacno;
    }
}
