package com.example.email;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.email.data.Data;
import com.example.email.data.RestService;
import com.example.email.data.Url;
import com.example.email.model.Account;
import com.example.email.model.Contact;
import com.example.email.model.Photo;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreateContactActivity extends AppCompatActivity {

    EditText etIme;
    EditText etPrezime;
    EditText etDisplay;
    EditText etEmail;
    int id;
    String ime;
    String prezime;
    String display;
    String email;
    ImageView contactImage;
    Button chooseImage;
    String photoPath = "";
    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    Contact newContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        chooseImage = findViewById(R.id.btnChoose);
        contactImage = findViewById(R.id.iwCreateContact);

        chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_DENIED){
                        //permission not granted
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};

                        requestPermissions(permissions, PERMISSION_CODE);

                    }
                    else{
                        //permission already granted
                        pickImageFromGallery();
                    }
                }
                else {
                    //system os prob
                    pickImageFromGallery();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_contact_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.cancelCreateContact:
                Intent in = new Intent(this,ContactsActivity.class);
                startActivity(in);
                return true;
            case R.id.saveCreateContact:
                //Toast.makeText(CreateContactActivity.this, "Sacuvano! ", Toast.LENGTH_SHORT).show();
                //createContact();
                etIme = findViewById(R.id.etCreateContactIme);
                etPrezime = findViewById(R.id.etCreateContactPrezime);
                etDisplay = findViewById(R.id.etCreateContactDisplay);
                etEmail = findViewById(R.id.etCreateContactEmail);



                ime = etIme.getText().toString();
                prezime = etPrezime.getText().toString();
                display = etDisplay.getText().toString();
                email = etEmail.getText().toString();

                if(ime.equals("")){
                    Toast.makeText(this, "Ime kontakta ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(prezime.equals("")){
                    Toast.makeText(this, "Prezime kontakta ne sme ostati prazno!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(display.equals("")){
                    Toast.makeText(this, "Displej kontakta ne sme ostati prazan!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if(email.equals("")){
                    Toast.makeText(this, "Email kontakta ne sme ostati prazan!", Toast.LENGTH_SHORT).show();
                    return true;
                }

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                RestService restService = retrofit.create(RestService.class);

                String loggedIn = LoginActivity.sharedPreferences.getString("display", "");

                newContact = new Contact();
                id = hashCode();
                newContact.setId(id);
                newContact.setFirstName(ime);
                newContact.setLastName(prezime);
                newContact.setDisplay(display);
                newContact.setEmail(email);


                Call<Contact> call = restService.createContact(id,ime,prezime,email,display,loggedIn);

                Log.i("PHOTO PATHHHHHHHH",photoPath);

                call.enqueue(new Callback<Contact>() {
                    @Override
                    public void onResponse(Call<Contact> call, Response<Contact> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE", Integer.toString(response.code()));
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<Contact> call, Throwable t) {
                        Log.i("STACK","TRACE CONTACT");
                        t.printStackTrace();
                        return;
                    }
                });

                if(photoPath.equals("")){
                    Toast.makeText(this, "Morate odabrati fotografiju!", Toast.LENGTH_SHORT).show();
                    return true;
                }

                Log.i("photo PATHTHTHTHT", photoPath);
                Photo p = new Photo();
                p.setId(hashCode());
                p.setPath(photoPath);
                p.setContact(newContact.getId());
                newContact.setPhoto(p);

                String enc = Base64.encodeToString(photoPath.getBytes(),
                    Base64.NO_WRAP);

                Log.i("encccc",enc);


                Retrofit retrofit1 = new Retrofit.Builder()
                        .baseUrl(Url.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                RestService restService1 = retrofit1.create(RestService.class);

                Call<Photo> call1 = restService1.createPhoto(p.getId(),enc,p.getContact());

                call1.enqueue(new Callback<Photo>() {
                    @Override
                    public void onResponse(Call<Photo> call, Response<Photo> response) {
                        if (!response.isSuccessful()){
                            Log.i("STATUS CODE PHOTO", Integer.toString(response.code()));
                            return;
                        }

                    }

                    @Override
                    public void onFailure(Call<Photo> call, Throwable t) {
                        Log.i("STACK","TRACE photo");
                        t.printStackTrace();
                        return;
                    }
                });
                for(Account a : Data.accounts){
                    if(a.getUsername().equals(loggedIn)){
                        a.getContacts().add(newContact);
                    }
                }
                Data.contacts.add(newContact);

                Intent inm = new Intent(this,ContactsActivity.class);
                startActivity(inm);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void pickImageFromGallery(){
        Intent in = new Intent(Intent.ACTION_PICK);
        in.setType("image/*");
        startActivityForResult(in, IMAGE_PICK_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSION_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //permission granted
                    pickImageFromGallery();
                }
                else{
                    //denied
                    Toast.makeText(this,"Permission denied...",Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE && data!=null){
            contactImage.setImageURI(data.getData());


            photoPath = data.getData().toString();

        }
    }



}
